import { LightningElement, track } from 'lwc';

import getAddress from "@salesforce/apex/AddressController.getAddress";

export default class Utility extends LightningElement {

    @track showSpinner;
    @track businessMAddress;
    accountId = '001r000000ZFw0eAAD'; // Specify the accountId

    connectedCallback(){
        this.getMergedAddress();
    }

    getMergedAddress(){
        getAddress({
            accountId: this.accountId
        })
        .then(result => {
            if (result != null) {
                for (var addr = 0; addr < result.length; addr++) {
                    let address = result[addr];
                        let addressApp = mergeAddressFields(address);
                        this.businessMAddress = addressApp != null ? addressApp.join(", ") : [];
                }
            }
        })
        .catch(error => {
            console.log('error is '+error);
        })
        .finally(() => this.showSpinner = false);
    }
    
    mergeAddressFields(address) {
        let addressApp;
        if (address && (address.street1 || address.street || address.city || address.state ||
                address.postalCode || address.country || address.street2 || address.physicalStreetLine1 || address.physicalStreetLine2 || address.physicalCity || address.physicalCountry ||
                address.physicalState || address.physicalZipCode)) {
            addressApp = [address.street1, address.street2, address.street, address.city, address.state, address.country, address.postalCode, address.physicalStreetLine1, address.physicalStreetLine2, address.physicalCity, address.physicalCountry,
                address.physicalState, address.physicalZipCode
            ];
            addressApp = addressApp.filter(function (el) {
                return el != null && el !== "";
            });
            return addressApp;
        }
        return null;
    }
}