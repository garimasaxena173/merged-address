public with sharing class AddressController {

    @AuraEnabled
    public static List < AddressWrapper > getAddress(String accountId) {
        try {
            if (!String.isEmpty(accountId)) {
                List<AddressWrapper> wrapList = new List<AddressWrapper> ();
                List < Account > accountList = new List < Account > ();
                accountList = getAddressRecord(accountId);
                
                if (!accountList.isEmpty()) {
                    for(Account acc : accountList){
                        AddressWrapper addressRecord = new AddressWrapper ();
                        addressRecord.city = acc.BillingCity;
                        addressRecord.street = acc.BillingState;
                        addressRecord.city = acc.BillingStreet;
                        addressRecord.postalCode = acc.BillingPostalCode;
                        wrapList.add(addressRecord);
                    }
                    return wrapList;
                }
            }
            return null;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    public static List<Account> getAddressRecord(String accountId){
        return [SELECT Id, BillingCity, BillingState,  BillingStreet, BillingPostalCode
        FROM Account
        WHERE Id = :accountId
        WITH SECURITY_ENFORCED];
    }

    public class AddressWrapper {
        @AuraEnabled public string city;
        @AuraEnabled public string street;
        @AuraEnabled public string city;
        @AuraEnabled public string postalCode;
    }
}
